package main

import (
	"log"
	"os"

	pb "gitlab.com/hello-world4/shippy-service-user/proto/user"
	micro "github.com/micro/go-micro/v2"
	microclient "github.com/micro/go-micro/v2/client"
	"golang.org/x/net/context"
)

func main() {
	srv := micro.NewService(
		micro.Name("shippy.cli.user"),
		micro.Version("latest"),
	)
	srv.Init()

	userService := pb.NewUserService("shippy.service.user", microclient.DefaultClient)

	name := "onymax"
	email := "patriot3308@gmail.com"
	password := "12345678"
	company := "onymax co"
	
	response, err := userService.Create(context.TODO(), &pb.User{
		Name:     name,
		Company:  company,
		Email:    email,
		Password: password,
	})
	if err != nil {
		log.Printf("Could not create: %v", err)
	}
	log.Printf("created: %s", response.User.Id)

	getAll, err := userService.GetAll(context.Background(), &pb.Request{})
	if err != nil {
		log.Fatalf("could not list useres: %v", err)
	}
	for _, v := range getAll.Users {
		log.Println(v)
	}

	authResponse, err := userService.Auth(context.TODO(), &pb.User{
		Email:    email,
		Password: password,
	})

	if err != nil {
		log.Fatalf("Could not authenticate user: %s error: %v\n", email, err)
	}

	log.Printf("Your access token: %s\n", authResponse.Token)
	os.Exit(0)
}