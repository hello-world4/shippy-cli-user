module gitlab.com/hello-world4/shippy-cli-user

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

go 1.15

require (
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/micro/go-micro/v2 v2.9.1
	gitlab.com/hello-world4/shippy-service-user v0.0.0-20210211104151-c67c4b7380a3
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	google.golang.org/genproto v0.0.0-20210207032614-bba0dbe2a9ea // indirect
)
